#!/bin/bash

USER=$( whoami )
FILEDIR="/home/${USER}/.config/argos/"
FILENAME="IPtoLOC*"
OLDFILENAME="IPtoLOC.r.30*"
TARGET="${FILEDIR}${FILENAME}"

if [ -f $OLDFILENAME ] ; then
    rm $OLDFILENAME  
fi

if [ -f $TARGET ] ; then
    rm $TARGET    
fi

cp $FILENAME $FILEDIR
chmod u+x $TARGET
