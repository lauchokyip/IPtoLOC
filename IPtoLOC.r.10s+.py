#!/usr/bin/env python3

import requests
import json
import urllib
# import subprocess

URL = "http://ip-api.com/json/"
# bashCommand = "windscribe status"




def check_internet_connection():
    try:
        response = urllib.request.urlopen('http://216.58.192.142',timeout=1.5)
        return True
    except urllib.error.URLError as err:
        return False


if check_internet_connection() is False:
    print("Connecting...")
else:
    response = requests.get(URL)
    json_output = json.loads(response.content.decode('utf-8'))
    print("{} {}".format(json_output["regionName"],json_output["query"]))
    print("---")

    # process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    # output, error = process.communicate()
    # print("Windscribe VPN Status: ")
    
    # if(str(output).find("DISCONNECTED") is not -1):
    #     print("Disconnected")
    # else:
    #     print("Connected")

    print("Thanks for using!")
    # print("Terminal")
    # print("--Launch Terminal | userMarkup=false iconName=utilities-terminal bash='gnome-terminal' terminal=false")
